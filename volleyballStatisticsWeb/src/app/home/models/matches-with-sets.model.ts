import { Set } from './set.model';

export interface MatchesWithSets {
    id: number;
    date: Date;
    oponnenent: string;
    sets: Set[];
}