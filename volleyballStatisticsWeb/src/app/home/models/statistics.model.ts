export interface Statistics {
    element: string;
    elementsList: ElementsList[];
}

export interface ElementsList {
    grade: string;
    value: string;
    result: string;
}