import { NgModule } from "@angular/core";
import { LoginComponent } from "./components/login/login.component";
import { HomeComponent } from "./components/home/home.component";
import { HomeService } from "./services/home.service";
import { UserInfoService } from "./services/user-info.services";
import { CommonModule } from "@angular/common";
import { MatToolbarModule } from "@angular/material/toolbar";
import { HomeRoutingModule } from "./home-routing.module";
import { MatButtonModule } from "@angular/material/button";
import { MatFormFieldModule } from "@angular/material/form-field";
import { ReactiveFormsModule } from "@angular/forms";
import { MatIconModule } from "@angular/material/icon";
import { MatInputModule } from "@angular/material/input";
import { StatisticsComponent } from "./components/statistics/statistics.component";
import { HttpClientModule } from "@angular/common/http";
import { MatSidenavModule } from "@angular/material/sidenav";
import { MatTreeModule } from "@angular/material/tree";
import { MatProgressBarModule } from "@angular/material/progress-bar";
import { MatListModule } from "@angular/material/list";
import {
  DynamicDatabase,
  DynamicDataSource
} from "./components/statistics/class/dynamic-flat-node";
import { StatisticElementComponent } from "./components/statistics/statistic-element/statistic-element.component";
import { MatDividerModule } from "@angular/material/divider";
import { ChartsModule } from "ng2-charts";

@NgModule({
  declarations: [
    LoginComponent,
    HomeComponent,
    StatisticsComponent,
    StatisticElementComponent
  ],
  imports: [
    HomeRoutingModule,
    CommonModule,
    MatToolbarModule,
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    ReactiveFormsModule,
    MatSidenavModule,
    HttpClientModule,
    MatTreeModule,
    MatProgressBarModule,
    MatDividerModule,
    ChartsModule,
    MatListModule
  ],
  providers: [UserInfoService, HomeService, DynamicDataSource, DynamicDatabase]
})
export class HomeModule {}
