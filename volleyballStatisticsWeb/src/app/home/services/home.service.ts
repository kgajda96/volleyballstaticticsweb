import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ApiUrl } from "../const/api-url.const";
import { HttpClient } from "@angular/common/http";
import { LoginModel } from "../models/login.model";
import { match } from 'minimatch';

@Injectable({
  providedIn: "root"
})
export class HomeService {
  private serverUrl = ApiUrl;
  constructor(private http: HttpClient) {}

  getTeams(login: string): Observable<any> {
    return this.http.get(this.serverUrl + "team/" + login);
  }

  getPlayers(login: string): Observable<any> {
    return this.http.get(this.serverUrl + "players/" + login);
  }

  login(loginInfo: LoginModel): Observable<any> {
    return this.http.post(this.serverUrl + "users/check", loginInfo);
  }

  getMatchesWithSets(login: string): Observable<any> {
    return this.http.get(this.serverUrl + "match/matches-with-sets/" + login);
  }

  getStatisticsMatch(matchId: number): Observable<any> {
    return this.http.get(this.serverUrl + "statistics/match/" + matchId);
  }

  getStatisticsSet(setId: number): Observable<any> {
    return this.http.get(this.serverUrl + "statistics/set/" + setId);
  }

  getStatisticsPlayer(playerId: number, matchId: number): Observable<any> {
    return this.http.get(this.serverUrl + "statistics/player/" + playerId + '/' + matchId);
  }
}
