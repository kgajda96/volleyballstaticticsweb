import { Component, OnInit } from "@angular/core";
import {
  DynamicFlatNode,
  DynamicDataSource,
  DynamicDatabase
} from "./class/dynamic-flat-node";
import { FlatTreeControl } from "@angular/cdk/tree";
import { HomeService } from "../../services/home.service";
import { UserInfoService } from "../../services/user-info.services";
import { MatchPlayers } from "../../models/match-players";
import { MatchesWithSets } from "../../models/matches-with-sets.model";
import { Statistics } from "../../models/statistics.model";
import { truncateSync } from "fs";

@Component({
  selector: "app-statistics",
  templateUrl: "./statistics.component.html",
  styleUrls: ["./statistics.component.scss"]
})
export class StatisticsComponent implements OnInit {
  matchesWithSets: MatchesWithSets[];
  players: MatchPlayers[];
  statistics: Statistics[];

  constructor(
    database: DynamicDatabase,
    private homeService: HomeService,
    private userInfoService: UserInfoService
  ) {
    this.treeControl = new FlatTreeControl<DynamicFlatNode>(
      this.getLevel,
      this.isExpandable
    );
    this.dataSource = new DynamicDataSource(this.treeControl, database);
    this.dataSource.data = database.initialData();
  }

  ngOnInit(): void {
    this.homeService
      .getPlayers(this.userInfoService.userInfo.login)
      .subscribe((players: MatchPlayers[]) => {
        this.players = players;
        this.dataSource.setPlayers(
          "Statystyki zawodnika",
          players.map((element: MatchPlayers) => element.number)
        );
      });

    this.homeService
      .getMatchesWithSets(this.userInfoService.userInfo.login)
      .subscribe((matchesWithSets: MatchesWithSets[]) => {
        this.matchesWithSets = matchesWithSets;
        this.dataSource.setPlayers(
          "Statystyki zespołowe",
          matchesWithSets.map((match: MatchesWithSets) => {
            const date = new Date(match.date);
            return (
              match.oponnenent +
              ": " +
              date.getDay() +
              "-" +
              date.getMonth() +
              "-" +
              date.getFullYear()
            );
          })
        );
      });
  }

  treeControl: FlatTreeControl<DynamicFlatNode>;

  dataSource: DynamicDataSource;

  getLevel = (node: DynamicFlatNode) => node.level;

  isExpandable = (node: DynamicFlatNode) => node.expandable;

  hasChild = (_: number, _nodeData: DynamicFlatNode) => _nodeData.expandable;

  setStatistic(node: string | number) {
    if (typeof node === "number") {
      this.getStatisticsOfPlayer(
        this.players.find(player => player.number === node).id,
        this.matchesWithSets[9].id
      );
    } else {
      this.getStatisticsOfMatch(
        this.matchesWithSets.find(match => {
          const date = new Date(match.date);
          let name =
            match.oponnenent +
            ": " +
            date.getDay() +
            "-" +
            date.getMonth() +
            "-" +
            date.getFullYear();
          if (name === node) {
            return true;
          }
          return false;
        }).id
      );
    }
  }

  private getStatisticsOfMatch(matchId): void {
    this.homeService
      .getStatisticsMatch(matchId)
      .subscribe((statistics: Statistics[]) => {
        this.statistics = statistics;
        this.sortStatistics();
      });
  }

  private getStatisticsOfSet(setId): void {
    this.homeService
      .getStatisticsSet(setId)
      .subscribe((statistics: Statistics[]) => {
        this.statistics = statistics;
        this.sortStatistics();
      });
  }

  private getStatisticsOfPlayer(playerId, matchId): void {
    this.homeService
      .getStatisticsPlayer(playerId, matchId)
      .subscribe((statistics: Statistics[]) => {
        this.statistics = statistics;
        this.sortStatistics();
      });
  }

  private sortStatistics() {
    this.statistics.map(element => {
      return element.elementsList.sort(function(a, b) {
        if (parseInt(a.grade) > parseInt(b.grade)) {
          return -1;
        }
        if (parseInt(b.grade) > parseInt(a.grade)) {
          return 1;
        }
        return 0;
      });
    });
  }
}
