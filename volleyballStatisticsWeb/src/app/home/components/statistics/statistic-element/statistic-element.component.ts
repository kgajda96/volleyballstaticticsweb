import { Component, OnInit, Input } from "@angular/core";
import { ChartDataSets, ChartOptions } from "chart.js";
import { Label, Color } from "ng2-charts";
import { Statistics, ElementsList } from "src/app/home/models/statistics.model";
import { element } from "protractor";

@Component({
  selector: "app-statistic-element",
  templateUrl: "./statistic-element.component.html",
  styleUrls: ["./statistic-element.component.scss"]
})
export class StatisticElementComponent implements OnInit {
  @Input() statistics: Statistics[];

  bubbleChartOptions: ChartOptions = {
    responsive: true,
    scales: {
      xAxes: [
        {
          ticks: {
            autoSkip: false,
            maxRotation: 90,
            minRotation: 90
          }
        }
      ]
    }
  };

  dataFromApiArray = [];

  barChartData: ChartDataSets[] = [{ data: [] }];

  barChartLabels: Label[] = [];

  barChartColors: Color[] = [
    {
      borderColor: "black",
      backgroundColor: "rgba(255,255,0,0.28)"
    }
  ];

  barChartLegend = true;
  barChartPlugins = [];
  barChartType = "bar";

  barChartOptions = {
    responsive: true
  };

  xAxis: {
    axisLabel: "X Axis";
    rotateLabels: 90;
  };

  options: {
    scales: {
      xAxes: [
        {
          type: "string";
        }
      ];
    };
  };
  constructor() {}

  ngOnInit() {
  }

  getValue(elementsList: ElementsList[]): number[] {
    return elementsList.map(el =>
      parseInt(el.value.substring(0, el.value.length - 1))
    );
  }

  getGrade(elementsList: ElementsList[]): string[] {
    return elementsList.map(el => el.grade);
  }
}
