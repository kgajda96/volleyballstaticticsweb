import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { LoginModel } from "../../models/login.model";
import { HomeService } from "../../services/home.service";
import { Router } from "@angular/router";
import { UserInfoService } from "../../services/user-info.services";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"]
})
export class LoginComponent implements OnInit {
  hide = true;
  loginForm: FormGroup;
  constructor(
    private formBuilder: FormBuilder,
    private homeService: HomeService,
    private router: Router,
    private userInfoService: UserInfoService
  ) {}

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      login: ["", Validators.required],
      password: ["", Validators.required]
    });
  }

  login(): void {
    this.homeService
      .login(this.prepareLoginModel())
      .subscribe((teamName: any) => {
        this.userInfoService.setUserInfo(this.prepareLoginInfo(teamName));
        this.router.navigate(["/statistics"]);
      });
  }

  private prepareLoginModel(): LoginModel {
    return {
      login: this.loginForm.get("login").value,
      password: this.loginForm.get("password").value
    };
  }

  private prepareLoginInfo(teamName): LoginModel {
    return {
      login: this.loginForm.get("login").value,
      password: this.loginForm.get("password").value,
      teamName: teamName.name
    };
  }
}
