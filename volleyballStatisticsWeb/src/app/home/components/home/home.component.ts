import { Component, OnInit } from "@angular/core";
import { UserInfoService } from "../../services/user-info.services";
import { LoginModel } from "../../models/login.model";
import { Router } from '@angular/router';

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"]
})
export class HomeComponent implements OnInit {
  loginInfo: LoginModel;

  constructor(private userInfoService: UserInfoService, private router: Router) {}

  ngOnInit() {
    this.userInfoService.getUserInfo().subscribe((loginInfo: LoginModel) => {
        this.loginInfo = loginInfo;
    });
  }

  logout(): void {
    this.router.navigate(['/login']);
  }
}
